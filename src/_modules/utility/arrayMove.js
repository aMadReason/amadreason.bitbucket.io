/**
 * Array Moving
 * const array = [ 1, 2, 3, 4, 5];
 * console.log(arrayInsert({ array, index: 2, items: [9,9,9]}));
 * console.log(arrayInsert({ array, index: 2, item: 10}));
 */
const arrayMove = ({
  array = null, from = null, to = null
}) => {
  const length = array.length-1;
  if (from > length || from < 0) return console.warn("'from' parameter is out of bounds");
  if (to > length || to < 0) return console.warn("'to' parameter is out of bounds");

  const arr = [...array];
  const item = arr.splice(from, 1)[0];
  return [    
    ...arr.splice(0, to, 1),
    item,
    ...arr.splice(to),
  ];
};
export default arrayMove;