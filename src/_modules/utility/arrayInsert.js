/**
 * Array Inserting
 * const array = [ 1, 2, 3, 4, 5];
 * console.log(arrayInsert({ array, index: 2, items: [9,9,9]}));
 * console.log(arrayInsert({ array, index: 2, item: 10}));
 */
const arrayInsert = ({
  array = null, index = null, items = null, item = null, replace = false
}) => {
  const arr = [...array]; // clone array so non-destructive
  if (!arr || index === false || (!items && !item)) return console.warn('Bad parameter list.');

  const itemsArray = items || [item]; // standardise items
  const delCount = replace  && itemsArray.length;
  const removed = arr.splice(index, delCount, ...itemsArray);

  return arr;
};
export default arrayInsert;