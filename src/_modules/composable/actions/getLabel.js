const getLabel = (thing) => ({
	getLabel: () => thing.props.label
});
export default getLabel;