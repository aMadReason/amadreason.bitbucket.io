const use = (thing) => ({

	use: ({ after = false, before = false, used = false } = {}) => {
    const { usage_count = false, usage_limit = false } = thing.props;
    if(usage_count === false || usage_count === false) throw Error('No usage props');  

    if(before) before(thing);

    if (usage_count >= usage_limit) return used && used(thing);
    thing.props.usage_count ++;    
    if (thing.props.usage_count >= thing.props.usage_limit) used && used(thing);    

    return after && after(thing);
  },

  usesLeft: () => {
    const { usage_count = false, usage_limit = false } = thing.props;
    if(usage_count === false || usage_count === false) throw Error('No usage props');
    return (usage_limit || 0) - usage_count;
  }

});

export default use;

