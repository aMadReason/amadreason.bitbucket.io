const display = (thing) => ({
	display: () => thing.props.icon
});

export default display;
