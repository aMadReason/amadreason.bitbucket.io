const describe = (thing) => ({
	describe: () => {
    const { description = false } = thing.props;
    if (!description) throw Error('No description prop'); 
    return description;
  }
});

export default describe;
