import Thing from '../Thing';

 class Item extends Thing {
  constructor(props) {
    super(props);
    this.props = {
      icon: null,
      usage_limit: null,
      usage_count: 0,
      spoil_at: null,
      weight: null,
      volume: null,
      ...props
    };
  }

}

export default Item;