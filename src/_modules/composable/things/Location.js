import Thing from '../Thing';

class Location extends Thing {
  constructor(props) {
    super(props);
    this.props = {
      icon: null,
      ...props
    };   

    this.items = [
      ...(props.items || [])
    ]
  }

  addItems(items = []) {
    this.items = [
      ...this.items,
      ...items
    ];
    return this;
  }

  getItems() {
    return this.items;
  }

}

export default Location;