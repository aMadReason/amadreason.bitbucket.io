

import Thing from '../Thing';

class Narrative extends Thing {
  constructor(props) {
    super(props);
    this.props = {
      ...props
    };   
  }
}

export default Narrative;