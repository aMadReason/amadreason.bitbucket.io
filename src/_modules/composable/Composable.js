// Composable class
// Master Class
// Dynamically add different methods for entities with the same data
// Useful if structure is likly to deviate form traditional hierarchical structure or you have many entities likly to have the same data/structure but different fuctionality.

 class Composable {
	can(action) {		
		if(typeof action === 'object') {
			let actionList = {};
			Object.keys(action).map(a => (actionList = { ...actionList, ...action[a](this)}));
			return Object.assign(this, actionList);
		}
				
		return Object.assign(this, action(this));
	}
}

export default Composable;