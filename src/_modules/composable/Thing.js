import Composable from './Composable';
import {uuid}  from '../utility';

// Thing class with basic state management for narrative
export default class Thing extends Composable {
	constructor(props) {
    super(props);
    const { states } = props;
    this.props = {
      label: null,
      name: null,
      description: null,
      ...props
    };

    this.uuid = uuid();
    this.active_state = 'default';
    this.states = {
      default: '',
      ...states
    };
	}

  get state() {
    return this.getState();
  }

  get stateKey() {
    return this.active_state;
  }

  switchState(key) {
    const stateKeys = Object.keys(this.states)
    if(stateKeys.includes(key)) this.active_state = key;
  }

  getState() {
    return this.states[this.active_state];
  }

  addState({ key, description }) {
    const newStates = {
      ...this.states,
      [key]: description 
    };
    this.states = newStates;
  }

  addStates(states = []) {
    states.map(({ key, description}) => this.addState({ key, description }));
    return this;
  }
}

