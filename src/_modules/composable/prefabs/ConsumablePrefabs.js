import { Item, Location } from '../things';
import { getName,getLabel, use, display } from '../actions';
import icon from '../icons/potion-ball.js';

const ConsumablePrefabs = key => ({

    'healing potion': new Item({      
      name: key, 
      label: 'Lesser Healing Potion',
      usage_limit: 1, 
      icon 
    }).addStates([
      { key: 'default', description: `A small glass vial of red liquid`},
      { key: 'used', description: `It's empty`}
    ]).can([
      getName,
      use,
      display,
      getLabel
    ])

}[key] || null);


export default ConsumablePrefabs;