import { Item, Location } from '../things';
import { getName,getLabel, use, display, describe, parse } from '../actions';
import icon from '../icons/potion-ball.js';

import { ConsumablePrefabs } from './';

const LocationPrefabs = key => ({
  
    'room': new Location({      
      name: key, 
      label: 'Room'
    }).addStates([
      { key: 'default', description: `A small room`},
    ]).can([
      getLabel, 
      describe,
      parse
    ]).addItems([
      ConsumablePrefabs('healing potion')
    ])

}[key] || null);


export default LocationPrefabs;