import './style.css';

import challenges from './challenges';
import experiments from './experiments';

const instances = [...challenges, ...experiments];

const appDiv = document.getElementById('app');

appDiv.innerHTML = `
<nav class="nav">
  ${ instances.map((c, i) => `<div class="card"><a href="#instance_${i}" title="${c.name}">&#8226;</a></div>`).join('')}
</nav>    
  <section tabindex="0" data-instance id="home">
      <div class="controls">
        <h1>Challenges & more</h1>
        <p>
          Initially based on wesbos js30. Random experimentation & examples.
        </p>
        <p>
          <a href="https://bitbucket.org/aMadReason/amadreason.bitbucket.io/src" title="Bitbucket">Source</a>
          <a href="https://amadreason.bitbucket.io" title="Bitbucket">Demo</a>
        </p>
      </div>
      ${ instances.map((c, i) => `<div class="card"><a href="#instance_${i}" title="${c.name}">${c.name}</a></div>`).join('')}
  
  </section>
`;

instances.map((c, i) => {
  const { instance } = c;
  instance.id = `instance_${i}`;
  appDiv.appendChild(instance)
});


const toTop = document.createElement('a');
toTop.href = "#home";
toTop.text = 'Top';
toTop.id = "toTop";
appDiv.appendChild(toTop);

