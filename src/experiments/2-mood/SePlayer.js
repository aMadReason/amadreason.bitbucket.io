import { Howl, Howler } from 'howler';

const Player = {
  channels: { 1: false },
  tracks: {},
  effects: {},
  addChannel({ key }) {
    this.channels[key] = { track: false, volume: 1 };
    return this;
  },
  addTrack({ key, src = [], loop = true, autoplay = false }) {
    this.tracks[key] = new Howl({ src, loop, autoplay });
    return this;
  },
  addEffect({ key, src = [], loop = false, autoplay = false }) {
    this.effects[key] = new Howl({ src, loop, autoplay });
    return this;
  },
  mute() {
    Howler.mute();
    return this;
  },
  volume(volume) {
    Howler.volume(volume);
    return this;
  },
  setChannelVolume({ channel = 1, volume = 1, fade = false }) {
    const { channels } = this;
    const channelStr = channels[channel].track;
    //const trackObj = tracks[channelStr];
    const oldVol = this.channels[channel].volume;
    this.channels[channel].volume = volume;
    if (channelStr === false) return null;

    if (fade) return this.tracks[channelStr].fade(oldVol, volume, 1000);

    this.tracks[channelStr].volume(volume);
  },
  // unload() {
  //   Howler.unload();
  //   return this;
  // },
  play(options) {
    const { sfx = false, track = false, channel = 1, fade = true, volume = 1 } = options;
    if (track) return this.playTrack({ track, channel, fade });
    if (sfx) return this.playSFX({ sfx, volume });
  },
  stop({ channel = false, fade = true }) {
    const { channels, tracks } = this;
    const channelStr = channels[channel].track;
    const trackObj = tracks[channelStr];

    if (channelStr === false) return null;
    if (trackObj && fade) this.tracks[channelStr].fade(1, 0, 1000);
    if (trackObj && !fade) this.tracks[channelStr].stop();

    if (channelStr) this.channels[channel] = false;
  },
  playSFX({ sfx, volume }) {
    const { effects } = this;
    const effect = effects[sfx];
    if (!effect) return null;
    if (volume) effect.volume(volume);
    effect.play();
  },
  playTrack({ track, channel = 1, fade = true }) {
    const { tracks, channels } = this;
    const { track: channelStr, volume } = channels[channel];
    const trackObj = tracks[track];

    if (channelStr === undefined || trackObj === undefined) return null; // if channel or track does not exist do nothing
    if (channelStr === track) return null;

    if (channelStr && fade) {
      this.tracks[channelStr].fade(volume, 0, 1000);
    } else if (channelStr) {
      this.tracks[channelStr].stop();
    }

    if (fade) this.tracks[track].fade(0, volume, 1000);
    this.tracks[track].play();

    this.channels[channel] = { ...this.channels[channel], track }; //update channel with new track key
  }
};

export default Player;