import { makeInstance } from '../../util';
import Player from './SePlayer';
import './style.css';

const app = makeInstance('mood');

const getSound = name => `public/assets/sounds/${name}`;

Player
  .addChannel({ key: 1 })
  .addChannel({ key: 2 })
  .addTrack({ key: 'Low Note', src: [ getSound('Very_Low_Note.mp3') ] })
  .addTrack({ key: 'Horror Music', src: [ getSound('Horror_Spacial_Winds.mp3') ] })
  .addEffect({ key: 'Chest', src: [ getSound('kenney_rpgaudio/Audio/doorOpen_2.ogg') ] })
  .volume(0.5);

app.innerHTML = `
<div>

<div class="controls-grid">
  <h1 class="fullcol">Mood</h1>
  <div>    
    <form data-mood-channel="1">
      <strong>Channel 1</strong>
      <select>
      <option value="off">Off</option>
       ${ Object.keys(Player.tracks).map(key => `<option value="${key}">${key}</option>`)}
      </select>
      <button type="submit">Play</button>
    </form>

    <form data-mood-channel="2">
    <strong>Channel 2</strong>
    <select>
    <option value="off">Off</option>
    ${ Object.keys(Player.tracks).map(key => `<option value="${key}">${key}</option>`)}
    </select>
    <button type="submit">Play</button>
  </form>
  
  </div>

  
    
  <div>
    <label for="mastervol">Master Volume<input type="range" step="0.1" min="0.0" max="1" class="slider" id="mastervol"></label><br />
    <label for="channel1vol">Channel 1 Volume<input type="range" step="0.1" min="0.0" max="1" class="slider" id="channel1vol"></label><br />
    <label for="channel2vol">Channel 2 Volume<input type="range" step="0.1" min="0.0" max="1"  class="slider" id="channel2vol"></label><br />
    <label for="sfxvol">SFX Volume<input type="range" step="0.1" min="0.0" max="1" class="slider" id="sfxvol"></label>
  </div>
  <div>
    <strong>Sound Effects</strong><br />
    ${ Object.keys(Player.effects).map(key => `<button data-mood-sfx="${key}">${key}</button>`).join('')}
  </div>
</div>

<div class="page">
  lorem ipsum
</div>

</div>
`;

app.querySelector('#channel1vol').addEventListener('change', e => {
  const { target } = e;
  const volume = target.value;
  Player.setChannelVolume({ channel: 1, volume });
  //console.log('channel 1', volume);
});

app.querySelector('#channel2vol').addEventListener('change', e => {
  const { target } = e;
  const volume = target.value;
  Player.setChannelVolume({ channel: 2, volume });
  //console.log('channel 2', volume);
});

app.querySelector('#mastervol').addEventListener('change', e => {
  const { target } = e;
  const volume = target.value;
  Player.volume(volume);
  //console.log('channel 2', volume);
});

app.querySelectorAll('form').forEach(el => {
  const channel = el.getAttribute('data-mood-channel');
  el.addEventListener('submit', e => {
    e.preventDefault();
    const select = e.target[0];
    const track = select.value;
    if (track === 'off') return Player.stop({ channel });
    return Player.play({ track, channel });
  });
});

app.querySelectorAll('button[data-mood-sfx]').forEach(el => {
  el.addEventListener('click', e => {
    const sfx = e.target.getAttribute('data-mood-sfx');
    const volume = app.querySelector('#sfxvol').value;
    //console.log(volume);
    Player.play({ sfx, volume });
  });
});

export default app;