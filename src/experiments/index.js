import one from './1-composable';
import two from './2-mood';
import storyengine from './story_engine';
import dndshop from './dnd-shop';

export default [
  {
    name: 'Composable',
    instance: one
  },
  {
    name: 'Mood',
    instance: two
  },
  {
    name: 'Story engine',
    instance: storyengine
  },
  {
    name: 'DND Shop',
    instance: dndshop
  }
];