const tokens = {
  "Barrel": {
    filename: "Barrel.png",
    price: { g: 1, s: 2, c: 3 },
    desc: 'An empty wooden barrel.'
  },
  "Bed, large, messy": {
    filename: "Bed, large, messy.png",
    price: { g: 100, s: 0, c: 10 },
    desc: ''
  },
  "Bed, large": {
    filename: "Bed, large.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Bed, smaller": {
    filename: "Bed, smaller.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Bookcase, 1": {
    filename: "Bookcase, 1.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Bookcase, 2": {
    filename: "Bookcase, 2.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Bookcase, narrow": {
    filename: "Bookcase, narrow.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Chair, armchair, 1": {
    filename: "Chair, armchair, 1.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Chair, armchair, 2": {
    filename: "Chair, armchair, 2.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "CHair, long bench": {
    filename: "CHair, long bench.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Chair, plush": {
    filename: "Chair, plush.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Chair, sofa": {
    filename: "Chair, sofa.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Chair, stool": {
    filename: "Chair, stool.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Chair, wooden": {
    filename: "Chair, wooden.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Chest, long": {
    filename: "Chest, long.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Chest, tiny": {
    filename: "Chest, tiny.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Chest": {
    filename: "Chest.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Clothes rack": {
    filename: "Clothes rack.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Clutter, book": {
    filename: "Clutter, book.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Clutter, books, 1": {
    filename: "Clutter, books, 1.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Clutter, books, 2": {
    filename: "Clutter, books, 2.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Clutter, bowl": {
    filename: "Clutter, bowl.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Clutter, bra": {
    filename: "Clutter, bra.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Clutter, cauldron": {
    filename: "Clutter, cauldron.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Clutter, coat": {
    filename: "Clutter, coat.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Clutter, cup, 1": {
    filename: "Clutter, cup, 1.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Clutter, cup, 2": {
    filename: "Clutter, cup, 2.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Clutter, cups": {
    filename: "Clutter, cups.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Clutter, cushion": {
    filename: "Clutter, cushion.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Clutter, cutlery": {
    filename: "Clutter, cutlery.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Clutter, game board": {
    filename: "Clutter, game board.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Clutter, globe": {
    filename: "Clutter, globe.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Clutter, goblet": {
    filename: "Clutter, goblet.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Clutter, kitchenware": {
    filename: "Clutter, kitchenware.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Clutter, laboratory": {
    filename: "Clutter, laboratory.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Clutter, meat": {
    filename: "Clutter, meat.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Clutter, parchment stack": {
    filename: "Clutter, parchment stack.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Clutter, parchment": {
    filename: "Clutter, parchment.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Clutter, picture frames": {
    filename: "Clutter, picture frames.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Clutter, poultry": {
    filename: "Clutter, poultry.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Clutter, stein": {
    filename: "Clutter, stein.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Clutter, vegetables": {
    filename: "Clutter, vegetables.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Clutter, writing": {
    filename: "Clutter, writing.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Covered, 1": {
    filename: "Covered, 1.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Covered, 2": {
    filename: "Covered, 2.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Covered, 3": {
    filename: "Covered, 3.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Crate": {
    filename: "Crate.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Curtains, 1": {
    filename: "Curtains, 1.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Curtains, 2": {
    filename: "Curtains, 2.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Hearth fence": {
    filename: "Hearth fence.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Hearth, 1": {
    filename: "Hearth, 1.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Hearth, 2": {
    filename: "Hearth, 2.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Hearth, 3": {
    filename: "Hearth, 3.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Hearth, chimney": {
    filename: "Hearth, chimney.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Hearth, poker": {
    filename: "Hearth, poker.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Light, candelabrum, 1": {
    filename: "Light, candelabrum, 1.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Light, candelabrum, 2": {
    filename: "Light, candelabrum, 2.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Light, candle, 1": {
    filename: "Light, candle, 1.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Light, candle, 2": {
    filename: "Light, candle, 2.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Light, chandelier, 1": {
    filename: "Light, chandelier, 1.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Light, chandelier, 2": {
    filename: "Light, chandelier, 2.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Light, chandelier, 3": {
    filename: "Light, chandelier, 3.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Light, chandelier, 4": {
    filename: "Light, chandelier, 4.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Light, wall, 1": {
    filename: "Light, wall, 1.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Light, wall, 2": {
    filename: "Light, wall, 2.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Light, wall, 3": {
    filename: "Light, wall, 3.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Organ": {
    filename: "Organ.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Partition": {
    filename: "Partition.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Piano": {
    filename: "Piano.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Picture frame, 1": {
    filename: "Picture frame, 1.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Picture frame, 2": {
    filename: "Picture frame, 2.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Picture frame, 3": {
    filename: "Picture frame, 3.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Picture frame, 4": {
    filename: "Picture frame, 4.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Placemat, rectangle": {
    filename: "Placemat, rectangle.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Placemat, square": {
    filename: "Placemat, square.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Plants, 1": {
    filename: "Plants, 1.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Plants, 2": {
    filename: "Plants, 2.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Plants, 3": {
    filename: "Plants, 3.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Plants, 4": {
    filename: "Plants, 4.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Plants, 5": {
    filename: "Plants, 5.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Plants, 6": {
    filename: "Plants, 6.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Rug, 1": {
    filename: "Rug, 1.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Rug, 2": {
    filename: "Rug, 2.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Rug, 3": {
    filename: "Rug, 3.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Rug, 4": {
    filename: "Rug, 4.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Rug, 5": {
    filename: "Rug, 5.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Shelf": {
    filename: "Shelf.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Statue, bust": {
    filename: "Statue, bust.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Suit of armor": {
    filename: "Suit of armor.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Table, bedside": {
    filename: "Table, bedside.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Table, bend": {
    filename: "Table, bend.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Table, dining": {
    filename: "Table, dining.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Table, drawers": {
    filename: "Table, drawers.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Table, long, rounded": {
    filename: "Table, long, rounded.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Table, medium": {
    filename: "Table, medium.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Table, rounded, small": {
    filename: "Table, rounded, small.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Table, rounded": {
    filename: "Table, rounded.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Table, small, 1": {
    filename: "Table, small, 1.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Table, small, 2": {
    filename: "Table, small, 2.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Toilet, 1": {
    filename: "Toilet, 1.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Toilet, 2": {
    filename: "Toilet, 2.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Toilet, basin": {
    filename: "Toilet, basin.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Toilet, portable": {
    filename: "Toilet, portable.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Trophy, 1": {
    filename: "Trophy, 1.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Trophy, 2": {
    filename: "Trophy, 2.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Trophy, 3": {
    filename: "Trophy, 3.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Trophy, 4": {
    filename: "Trophy, 4.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Trophy, 5": {
    filename: "Trophy, 5.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Trophy, 6": {
    filename: "Trophy, 6.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Vanity": {
    filename: "Vanity.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Vase": {
    filename: "Vase.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Wall, door": {
    filename: "Wall, door.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Wall, filler": {
    filename: "Wall, filler.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Wall, secret door": {
    filename: "Wall, secret door.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Wall, window, alt": {
    filename: "Wall, window, alt.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Wall, window": {
    filename: "Wall, window.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Wardrobe, 1": {
    filename: "Wardrobe, 1.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Wardrobe, 2": {
    filename: "Wardrobe, 2.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Wardrobe, 3": {
    filename: "Wardrobe, 3.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Webs, 1": {
    filename: "Webs, 1.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Webs, 2": {
    filename: "Webs, 2.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Webs, 3": {
    filename: "Webs, 3.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Webs, 4": {
    filename: "Webs, 4.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  },
  "Webs, 5": {
    filename: "Webs, 5.png",
    price: { g: 0, s: 0, c: 0 },
    desc: ''
  }
}

export default tokens;