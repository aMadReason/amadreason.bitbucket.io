import swal from 'sweetalert2';
import { makeInstance } from '../../util';
import tokens from './tokens';
import './style.css';

const app = makeInstance('dnd-shop');
const getPath = name => `public/dnd-shop/MansionFurniture/Tokens/${name}`;

const basket = localStorage.getItem('basket') ? JSON.parse(localStorage.getItem('basket')) : { items: [] };

const renderItem = (k, t) => `
  <div>
    <strong>${ k}</strong>
    <img src="${ getPath(t.filename)}" />
    <button 
      data-item="${k}" 
      data-path="${getPath(t.filename)}" 
      data-g="${t.price.g}" 
      data-s="${t.price.s}" 
      data-c="${t.price.c}"
      data-desc="${t.desc}"
      >Info</button>
  </div>
`;

const renderBasket = () => {
  const total = { g: 0, s: 0, c: 0 };

  basket.items.map(item => {
    total.g = total.g + item.price.g;
    total.s = total.s + item.price.s;
    total.c = total.c + item.price.c;
  });

  return `
  <h1>DND Shop</h1>
  <strong>Total: </strong> G${total.g}, S${total.s}, C${total.c}
  <hr />
  <ul>
    ${basket.items.map((i, index) => `
      <li>
        ${i.item} 
        <div>G${i.price.g}, S${i.price.s}, C${i.price.c}</div>
        <button data-basketIndex="${index}">X</button>
      </li>
    `).join('')}
  </ul>
  <hr />
  <div><small>Assets from: <br /><a href="https://www.patreon.com/2minutetabletop" title="Assets from 2minutetabletop">2minutetabletop on Patreon</a></small></div>

`};

const renderApp = () => {
  app.innerHTML = `  
  <div class="basket">
    ${ renderBasket()}
  </div>  
  <div class="item-grid">
    ${ Object.keys(tokens).map(t => renderItem(t, tokens[t])).join('')}
  </div>`;
};

const updateBasket = () => {
  const basketEl = app.querySelector('.basket');
  basketEl.innerHTML = renderBasket();
  basketEl.querySelectorAll('button[data-basketIndex]').forEach(el => {
    el.addEventListener('click', e => {
      const { target } = e;
      console.log(123);
      const { basketIndex } = target.dataset;
      basket.items.splice(basketIndex, 1);
      updateBasket();
    });
  });

  localStorage.setItem('basket', JSON.stringify(basket));
};

renderApp();
updateBasket();

app.querySelectorAll('.item-grid button').forEach(el => {
  el.addEventListener('click', e => {
    const { target } = e;
    const { g, s, c, item, path, desc } = target.dataset;

    swal({
      title: item,
      html: `
        <div>
          <p>${desc}</p>
          <strong>Price:</strong> <em>G</em>${g}, <em>S</em>${s}, <em>C</em>${c}
        </div>
      `,
      text: "You won't be able to revert this!",
      imageUrl: path,
      imageWidth: 200,
      imageAlt: item,
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Add to cart'
    }).then((result) => {
      if (result.value) {
        swal(
          'Added',
          '',
          'success'
        );

        basket.items.push({ ...tokens[item], item });
        updateBasket();
      }
    })


  });
});

export default app;