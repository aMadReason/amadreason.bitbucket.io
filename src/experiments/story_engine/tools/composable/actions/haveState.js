const haveState = (thing) => ({
  state: 'default',  
  states: {
    default: 'default value'
  },
	haveState() {
    return true;
  },
  addState({ key, value }) {
    const exists = Object.keys(thing.states).includes(key);
    if (! exists) thing.states[key] = value;
  },
  updateState({ key, value }) {
    const exists = Object.keys(thing.states).includes(key);
    if (exists) thing.states[key] = value;
  },
  setState(key) {
    const exists = Object.keys(thing.states).includes(key);
    if (exists) thing.state = key;    
  },
  getState() {
    return { [thing.state] : thing.states[thing.state]};
  }
});

export default haveState;