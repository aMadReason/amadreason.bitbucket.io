const use = (thing) => ({
  usage_count: 0,
  usage_limit: null,
	use: ({ after = null, before = null, used = null } = {}) => {
    const { usage_count = null, usage_limit = null } = thing;

    if(usage_count === null || usage_count === null) throw Error('No usage props');  

    // before use hook
    if(before) before(thing);

    // if limit is reached do this
    if (usage_count >= usage_limit) return used && used(thing);

    // increment
    thing.usage_count ++;    

    // if limit is reached do this
    if (thing.usage_count >= thing.usage_limit) return used && used(thing);    

    // after use hook
    return after && after(thing);
  },
  setUsageLimit(number) {
    if (typeof number !== 'number') return null;
    thing.usage_limit = number;
  },
  usesRemaining() {
    const { usage_count = null, usage_limit = null } = thing;
    if(usage_count === null || usage_count === null) throw Error('No usage props');
    return (usage_limit || 0) - usage_count;
  }
});

export default use;

