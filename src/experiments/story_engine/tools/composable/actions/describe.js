const describe = (thing) => ({
  description: '',
	describe: () => {
    return thing.description;
  },
  setDescription(string) {
    thing.description = string;
  }
});

export default describe;