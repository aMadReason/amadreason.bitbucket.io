import { makeInstance } from '../../util';
import './style.css';

const app = makeInstance('story_engine');

export default app;