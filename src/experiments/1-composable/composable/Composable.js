const Composable = (name) => ({
  name,  
  can(action) {
    /// if single action 
    if (typeof action !== 'object') return Object.assign(this, action(this));

    // if multiple actions in obj
    let actionList = {};
    Object.keys(action).map(a => (actionList = { ...actionList, ...action[a](this)}));
    return Object.assign(this, actionList);
  }
});


export default Composable;