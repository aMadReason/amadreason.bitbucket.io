import { makeInstance } from '../../util'; 
// import { Composable, describe, haveState, use } from './composable';
import './style.css';

const app = makeInstance('composable');



// const test = new Composable('bob');


// test.can(describe).can(haveState).can(use);

// console.log(test);

// test.setUsageLimit(3);

// console.log(test.usesRemaining());
// test.use();
// test.use();
// console.log(test.usesRemaining());
// test.use();
// console.log(test.usesRemaining());

// test.setDescription('hello world');

// test.describe();

// test.updateState({ key: 'default', value: "locked" });
// test.addState({ key: 'broken', value: "false" });




const describe = (thing) => ({
  description: '',
	describe: () => {
    return thing.description;
  },
  setDescription(string) {
    thing.description = string;
  }
});


//console.log(test.state);
const Composable = () => ({
  actions: {},  
  attribs: {},
  can(newAction) {
    /// if single action 
    //if (typeof action !== 'object') return Object.assign(this, action(this));

    // if multiple actions in obj
    let { actions, attribs } = this;

    //Object.keys(action).map(a => (actionList = { ...actionList, ...action[a](this)}));

    Object.keys(newAction).map(a => {
      if (typeof a === 'function') return (actions[a] = newAction[a](this));
    });



    return Object.assign(this, { actions, attribs });
  }
});

const test = new Composable();

test.can(describe);

console.log(test);

app.innerHTML = `
  <div class="controls">Composable</div>

  <pre>  
${ test && JSON.stringify(test, null, 2) }
  </pre>
`;


export default app;