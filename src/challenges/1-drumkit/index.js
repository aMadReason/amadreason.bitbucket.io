import { makeInstance } from '../../util'; 
import './style.css';

const app = makeInstance('drumkit');

const getSound = name => `public/assets/sounds/${name}.wav`;

const sounds = [
  { name: 'boom', key: 'KeyZ' },
  { name: 'clap', key: 'KeyX' },
  { name: 'hihat', key: 'KeyC' },
  { name: 'kick', key: 'KeyV' },
  { name: 'openhat', key: 'KeyB' },
  { name: 'ride', key: 'KeyN' },
  { name: 'snare', key: 'KeyM' },
  { name: 'tink', key: 'Comma' },
  { name: 'tom', key: 'Period' }
];

const playSound = key => {
  if (!sounds.find(s => s.key === key)) return null;
  const sound = sounds.find(s => s.key === key);
  const el = app.querySelector(`[data-key="${key}"]`);
  el.setAttribute('data-playing', true);
  if (sound === undefined) return null;
  const audio = new Audio(getSound(sound.name));
  audio.addEventListener('ended', () => el.removeAttribute('data-playing'));
  audio.play();
};

app.addEventListener('keyup', e => playSound(e.code), { bubbles: true });
app.addEventListener('click', e => playSound(e.target.getAttribute('data-key')), { bubbles: true });

const control = `<div class="controls"><h1>Drumkit</h1></div>`;

const buttons = sounds.map(({ key, name }) => `
  <button data-key="${key}">${name}<br><small data-key="${key}">${key.replace('Key', '').toLocaleLowerCase()}</small></button>
  `).join('');

app.innerHTML = [control, buttons].join('');

export default app;