import { makeInstance } from '../../util'; 
import './style.css';

const app = makeInstance('cssvars');

const update = (e) => {
  const inputs = app.querySelectorAll('input');

  inputs.forEach(element => {
    const { name, value, dataset } = element;
    const { sizing: suffix = '' } = dataset;
    app.style.setProperty(`--${name}`, `${value}${suffix}`);
  });

};

app.addEventListener('change', update);
app.addEventListener('mouseup', update);

app.innerHTML = `

<div class="controls">
  <h1>Css Variables</h1>
  <hr />
  <div><label for="spacing">Spacing:</label> <input name="spacing" type="range" data-sizing="px" value="0"" min=0 /></div>
  <div><label for="blur">Blur:</label> <input name="blur" type="range" data-sizing="px" value="0"" min=0 /></div>
  <div><label for="opacity">Opacity:</label> <input name="opacity" type="range" min=0 max=1 step=0.1 value="1" /></div>
  <div><label for="base">Base:</label> <input name="base" type="color" /></div>
</div>
<img alt="" src="public/assets/img/baked-baking-cakes-1005406.jpg" />
`;

export default app;