import one from './1-drumkit';
import two from './2-clock';
import three from './3-cssvars';
import four from './4-arrays';
import five from './5-flexgallery';
import six from './6-ajaxhead';


export default [
  {
    name: 'Drumkit',
    instance: one
  },
  {
    name: 'Clock',
    instance: two
  },
  {
    name: 'Css Vars - image effect',
    instance: three
  },
  {
    name: 'Arrays - dnd weapons',
    instance: four
  },
  {
    name: 'Flex Gallery',
    instance: five
  },
  {
    name: 'Ajax Head',
    instance: six
  }
];