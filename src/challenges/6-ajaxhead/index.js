import { makeInstance } from '../../util'; 
import './style.css';

const app = makeInstance('ajaxhead');

app.tabIndex = 0;
app.innerHTML = `
  <div class="controls">
    <h1>Ajax Type Head</h1>
  </div>
`;

export default app;