import { makeInstance } from '../../util'; 
import './style.css';

const app = makeInstance('clock');

const setDate = () => {
  const hands = Array.from(document.querySelectorAll('[data-instance] .hand'));
  const secondHand = hands.find(el => el.className.includes('second-hand'));
  const minuteHand = hands.find(el => el.className.includes('min-hand'));
  const hourHand = hands.find(el => el.className.includes('hour-hand'));

  const now = new Date();
  const secs = now.getSeconds();
  const mins = now.getMinutes();
  const hour = now.getHours();

  if (secs === 0 || mins === 0 || hour === 12 || hour === 0) {
    hands.map(hand => hand.style.transition = 'none');
  }

  if (secs === 1 || mins === 1 || hour === 11 || hour === 1) {
    hands.map(hand => hand.style.transition = null);
  }

  const secsDegs = ((secs / 60) * 360) + 90;
  const minsDeg = ((mins / 60) * 360) + 90;
  const hoursDeg = ((hour / 12) * 360) + 90;

  secondHand.style.transform = `rotate(${secsDegs}deg)`;
  minuteHand.style.transform = `rotate(${minsDeg}deg)`;
  hourHand.style.transform = `rotate(${hoursDeg}deg)`;
};

setInterval(setDate, 1000)



app.innerHTML = `
  <div class="controls">
    <h1>Clock</h1>
  </div>
  <div class="clock-face">
    <div class="hand hour-hand"></div>  
    <div class="hand min-hand"></div> 
    <div class="hand second-hand"></div> 
  </div>
`;





export default app;