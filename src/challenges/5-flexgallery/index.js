import { makeInstance } from '../../util'; 
import './style.css';

const app = makeInstance('flexgallery');

app.tabIndex = 0;
app.innerHTML = `
  <div class="panels">
    <div class="panel panel1">
      <p>Hey</p>
      <p>Let's</p>
      <p>Dance</p>
    </div>
    <div class="panel panel2">
      <p>Give</p>
      <p>Take</p>
      <p>Receive</p>
    </div>
    <div class="panel panel3">
      <p>Experience</p>
      <p>It</p>
      <p>Today</p>
    </div>
    <div class="panel panel4">
      <p>Give</p>
      <p>All</p>
      <p>You Can</p>
    </div>  
    <div class="panel panel5">
      <p>Life</p>
      <p>In</p>
      <p>Motion</p>
    </div>  
  </div>
`;

const panels = app.querySelectorAll('.panel');
const toggleOpen = (target) => target.classList.toggle('open');
const toggleActive = ({ propertyName, target }) => {
  if (propertyName.includes('flex')) {
    target.classList.toggle('open-active')
  }
};

panels.forEach(p => p.addEventListener('click', () => toggleOpen(p)));
panels.forEach(p => p.addEventListener('transitionend', e => toggleActive(e)));

export default app;