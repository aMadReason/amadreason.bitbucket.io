import rawWeapons from './dndWeapons';
import { makeInstance } from '../../util'; 
import './style.css';

const app = makeInstance('arrays');





const sort = (arr) => arr.sort((a, b) => a.name > b.name ? 1 : -1);
const weapons = sort(rawWeapons);

const renderInfo = (info = false) => {
  const infoEl = app.querySelector('.info');
  if (!infoEl) return infoEl.innerHTML = '';
  infoEl.innerHTML = `${Object.keys(info).map(key => info[key] ? `<div class="card">${key}: ${info[key]}</div>` : '').join('')}`;
};

const render = (array = []) => {
  renderInfo();
  app.querySelector('.weapon-list').innerHTML = `${array.map(({ name }) => `<div data-name="${name}" class="card">${name}</div>`).join('')}`
};

const filter = (value, prop = 'name') => weapons.filter(w => {
  renderInfo();
  if (w[prop] === null || w[prop] === undefined) return false;
  if (prop === 'name') return w[prop].includes(value.toLowerCase().trim());
  return true;
});


app.innerHTML = [
  `<div class="controls">
    <h1>Arrays (sorting, filtering, rendering)</h1><hr>

    <label for="name">Name</label>
    <input name="name" type="text">

    <br><br>

    <fieldset>
      <legend>Filter By Prop</legend>
      <label for="none">Clear <input type="radio" id="none" name="props" value="none" /></label>
      <label for="heavy">Heavy <input type="radio" id="heavy" name="props" value="heavy" /></label>
      <label for="range">Range <input type="radio" id="range" name="props" value="range" /></label>    
      <label for="versatile">Versatile <input type="radio" id="versatile" name="props" value="versatile" /></label>          
    </fieldset>  
  
  </div>`,
  `<div class="weapon-list"></div>`,
  `<div class="info"></div>`
].join('');

render(weapons);


app.querySelector('input[type="text"]').addEventListener('keyup', e => {
  const val = e.target.value;
  render(filter(val, 'name'));
});

const props = Array.from(app.querySelectorAll('input[type="radio"]'));
props.map(p => {
  p.addEventListener('input', e => {
    const val = e.target.value;
    if (val === 'none') return render(weapons);
    render(filter(weapons, val));
  });
});


app.querySelector('.weapon-list').addEventListener('click', e => {
  const { dataset } = e.target;
  const { name: val } = dataset;

  console.log(val);

  if (!val) return null;
  renderInfo(weapons.find(w => w.name === val));
});


export default app;