const props = {
  bludgeoning: null,
  slashing: null,
  piercing: null,
  light: null,
  finesse: null,
  thrown: null,
  range: null,
  versatile: null,
  twoHanded: null,
  weight: null,
  damage: null
};

const bludgeoning = true;
const slashing = true;
const piercing = true;
const light = true;
const finesse = true;
const thrown = true;
const ammunition = true;
const twoHanded = true;
const loading = true;
const heavy = true;
const reach = true;
const special = true;


export const dnd_simple_melee = [
  { ...props, name: 'club', cost: '1sp', damage: '1d4', weight: '2lb', bludgeoning, light },
  { ...props, name: 'dagger', cost: '1gp', damage: '1d4', piercing, weight: '3lb', range: [20, 60], finesse, light, thrown },
  { ...props, name: 'great club', cost: '2sp', damage: '1d8', weight: '10lb', twoHanded },
  { ...props, name: 'hand axe', cost: '5gp', damage: '1d6', slashing, weight: '10lb', range: [20, 60], light, thrown },
  { ...props, name: 'javelin', cost: '5sp', damage: '1d6', piercing, weight: '2lb', range: [30, 120], thrown },
  { ...props, name: 'light hammer', cost: '2gp', damage: '1d4', bludgeoning, weight: '2lb', range: [20, 60], thrown, light },
  { ...props, name: 'mace', cost: '5gp', damage: '1d6', weight: '4lb', bludgeoning },
  { ...props, name: 'quarter staff', cost: '2sp', damage: '1d6', weight: '4lb', bludgeoning, versatile: '1d8' },
  { ...props, name: 'sickle', cost: '1gp', damage: '1d4', weight: '2lb', slashing, light },
  { ...props, name: 'spear', cost: '1gp', damage: '1d6', weight: '3lb', piercing, versatile: '1d8', thrown, range: [20, 60] }
];

export const dnd_simple_ranged = [
  { ...props, name: 'crossbow, light', cost: '25gp', damage: '1d8', piercing, weight: '5lb', range: [80, 320], ammunition, twoHanded, loading },
  { ...props, name: 'dart', cost: '5cp', damage: '1d4', piercing, weight: '.5lb', range: [20, 60], finesse, thrown },
  { ...props, name: 'shortbow', cost: '25gp', damage: '1d6', piercing, weight: '2lb', range: [80, 320], ammunition, twoHanded },
  { ...props, name: 'sling', cost: '1sp', damage: '1d4', bludgeoning, range: [30, 120], ammunition },
];

export const dnd_martial_melee = [
  { ...props, name: 'battleaxe', cost: '10gp', damage: '1d8', slashing, weight: '4lb', versatile: '1d10' },
  { ...props, name: 'flail', cost: '10gp', damage: '1d8', bludgeoning, weight: '2lb' },
  { ...props, name: 'glaive', cost: '20gp', damage: '1d10', slashing, weight: '6lb', heavy, reach, twoHanded },
  { ...props, name: 'greataxe', cost: '30gp', damage: '1d12', slashing, weight: '7lb', heavy, twoHanded },
  { ...props, name: 'greatsword', cost: '50gp', damage: '2d6', slashing, weight: '6lb', heavy, twoHanded },
  { ...props, name: 'halberd', cost: '20gp', damage: '1d10', slashing, weight: '6lb', heavy, reach, twoHanded },
  { ...props, name: 'lance', cost: '10gp', damage: '1d12', piercing, weight: '6lb', reach, special },
  { ...props, name: 'longsword', cost: '105p', damage: '1d8', slashing, weight: '3lb', versatile: '1d10' },
  { ...props, name: 'maul', cost: '10gp', damage: '2d6', bludgeoning, weight: '10lb', heavy, twoHanded },
  { ...props, name: 'morningstar', cost: '15gp', damage: '1d8', piercing, weight: '4lb' },
  { ...props, name: 'pike', cost: '5gp', damage: '1d10', piercing, weight: '18lb', heavy, reach, twoHanded },
  { ...props, name: 'rapier', cost: '25gp', damage: '1d8', piercing, weight: '2lb', finesse },
  { ...props, name: 'scimitar', cost: '25gp', damage: '1d6', slashing, weight: '3lb', finesse, light },
  { ...props, name: 'shortsword', cost: '10gp', damage: '1d6', piercing, weight: '2lb', finesse, light },
  { ...props, name: 'trident', cost: '5gp', damage: '1d6', piercing, weight: '4lb', range: [20, 60], thrown, versatile: '1d8' },
  { ...props, name: 'war pick', cost: '5gp', damage: '1d8', piercing, weight: '2lb' },
  { ...props, name: 'warhammer', cost: '15gp', damage: '1d8', bludgeoning, weight: '2lb', versatile: '1d10' },
  { ...props, name: 'whip', cost: '15gp', damage: '1d8', slashing, weight: '3lb', finesse, reach },
];

export const dnd_martial_ranged = [
  { ...props, name: 'blowgun', cost: '10gp', damage: '1', piercing, weight: '1lb', range: [25, 100], ammunition, loading },
  { ...props, name: 'crossbow, hand', cost: '50gp', damage: '1d6', piercing, weight: '3lb', range: [30, 120], ammunition, light, loading },
  { ...props, name: 'crossbow, heavy', cost: '50gp', damage: '1d10', piercing, weight: '18lb', range: [100, 400], ammunition, heavy, loading, twoHanded },
  { ...props, name: 'longbow', cost: '50gp', damage: '1d8', piercing, weight: '2lb', range: [120, 600], ammunition, heavy, twoHanded },
  { ...props, name: 'net', cost: '1gp', weight: '3lb', range: [5, 15], thrown, special },
];

export default [
  ...dnd_simple_melee,
  ...dnd_simple_ranged,
  ...dnd_martial_melee,
  ...dnd_martial_ranged
];