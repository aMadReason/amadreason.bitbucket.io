/*
Please ensure you have the following package installed globally:

** min versions
+-- eslint@5.8.0
+-- babel-eslint@10.0.1
+-- eslint-plugin-import@2.14.0
+-- eslint-plugin-jest@21.26.2
+-- eslint-plugin-jsx-a11y@6.1.2
+-- eslint-plugin-react@7.11.1
** Use command `npm list -g --depth=0` to view your global packages

When in doubt run the following:
  npm i -g eslint babel-eslint eslint-plugin-import eslint-plugin-jest eslint-plugin-jsx-a11y


If you are using vsCode please ensur eyou have the following plugin installed:
  https://marketplace.visualstudio.com/items?itemName=dbaeumer.vscode-eslint
And have enabled the eslint autofix on save rule in vsCode preferences.
*/

module.exports = {
  root: true,
  extends: ["eslint:recommended", "plugin:jsx-a11y/recommended"], // dont add the import recomends here, bad things happen
  env: {
    node: true,
    browser: true,
    es6: true,
    'shared-node-browser': true,
    'jest/globals': true
  },
  "parser": "babel-eslint",
  parserOptions: {
    ecmaVersion: 2018,
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true
    }
  },
  rules: {
    "array-bracket-newline": ["error", { "multiline": true }],
    "object-curly-newline": ["error", { "multiline": true }],
    "object-curly-spacing": ["error", "always"],
    "object-property-newline": ["error", { "allowAllPropertiesOnSameLine": true }],
    "no-param-reassign": "error", // trying to discourage mutation
    "no-useless-concat": "error",
    "array-bracket-spacing": ["error", "always"],
    "block-spacing": "error",
    "brace-style": ["error", "1tbs", { "allowSingleLine": true }],
    camelcase: ["error", { ignoreDestructuring: true }],
    "comma-dangle": ["error", "never"],
    "comma-spacing": ["error", { "before": false, "after": true }],
    "implicit-arrow-linebreak": ["error", "beside"],
    indent: ["error", 2],
    "key-spacing": ["error", { "beforeColon": false }],
    'max-len': ['error', { "code": 150, "ignoreTrailingComments": true }],
    "max-statements-per-line": ["error", { "max": 1 }],
    "new-parens": "error",
    "no-multiple-empty-lines": ["error", { max: 1 }],
    "no-trailing-spaces": "error",
    "no-unneeded-ternary": "error",
    "no-whitespace-before-property": "error",
    quotes: ["error", "single", { "avoidEscape": true }],
    semi: ["error", "always"],
    "semi-style": ["error", "last"],
    "space-in-parens": ["error", "never"],
    //es6+
    "arrow-spacing": ["error", { "before": true, "after": true }],
    "prefer-const": "error",
    "prefer-template": "error",
    "rest-spread-spacing": ["error", "never"],
    'import/no-extraneous-dependencies': ['error', { devDependencies: true }],


    // Used in previous config, here until new eslint is stable

    // "react/jsx-indent": ["error", indentSpaces],
    // "react/jsx-indent-props": ["error", indentSpaces],
    // 'class-methods-use-this': 0,
    // 'linebreak-style': 0,
    // 'arrow-parens': [0, 'as-needed', { requireForBlockBody: false }],
    // 'function-paren-newline': ['error', 'consistent'],
    // 'consistent-return': ['error', { treatUndefinedAsUnspecified: true }],
    // 'max-len': ['error', 150],
    // 'no-param-reassign': [2, { props: true }], // do not disable props as it cause sisues for widgets
    // 'comma-dangle': ['error', { functions: 'never' }],
    // 'no-return-assign': ['error', 'except-parens'],
    // 'no-underscore-dangle': [
    //   'error',
    //   { allowAfterThis: true, allowAfterSuper: true, allow: [] }
    // ],
    // "jsx-a11y/label-has-for": 0 // disabled this rule as we need to avoid using id's and it still errors for plain implicits
  },
  globals: {
    e: true,
    evt: true,
    event: true,
    document: true,
    window: true
  },
  plugins: [
    "jest",
    "react",
    "jsx-a11y",
    "import"
  ],
  "settings": {
    "react": {
      "version": "latest", // React version, default to the latest React stable release
    },
    "propWrapperFunctions": ["forbidExtraProps"]
  }
};

