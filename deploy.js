const { resolve} = require('path');
const { execSync, } = require('child_process');

// expects to be executed as follows: 
// node deploy.js { branch_name }
// node deploy.js master

function execute(cmd, msg = false) {
  try {
    execSync(cmd, {
      cwd: resolve(process.cwd()),
      stdio: 'inherit'
    });
    if (msg) console.log(msg);
  } catch (err) {
    err.stdout;
    err.stderr;
    err.pid;
    err.signal;
    err.status;
  }
}

try {
  const args = process.argv.slice(2); // 0 is path to node, 1 is path to script
  const branch = args[0] ;

  execute(`git pull origin ${branch}`, `>>> Pulling from ${branch} 🍗`);

  execute('npm run build', '>>> Ran Build 🔨');

  execute('git add ./dist', '>>> Added ./dist 📁');

  execute('git commit -m "Rebuilt"', '>>> Added message ✉️');

  execute(`git push origin ${branch}`, '>>> Done️ 👍');  

} catch(err) {
  console.log(err, '!!!! 😱 Deployment failure 😱 !!!!!');
}


