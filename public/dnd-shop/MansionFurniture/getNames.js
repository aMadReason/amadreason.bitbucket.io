const fs = require('fs');
const path = require('path');


const tokens = fs.readdirSync('./Tokens');

const tokenObj = {};

tokens.map(t => {
  tokenObj[t.replace('.png', '')] = {
    filename: t
  };
});



fs.writeFileSync(`${__dirname}/tokens.js`, ` export default ${JSON.stringify(tokenObj, null, 2)}`);

console.log(tokens);